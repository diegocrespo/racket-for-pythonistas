

# Some Math
1 + 1
10 / 2
5 ** 2
6 * 3
12 - 6
(8 * 6) + 12

# Booleans
True
False


# Strings
"Hello World"
"Aretha \"The Queen of Soul\" Franklin"
"Fee " + "Fi " + "Fo " + "Fum"
"BlackSmith"[2]
print("This is some text it has\n {}\n".format("Some Lines"))
len("grape")
"restaurant"[0:4]
print("onomatopoeia")

# lists
menu = ["eggs", "bacon", "sausage", "spam"]
menu[2]
len(menu)
menu[-1]


# removing things from lists
menu.remove("bacon") # removes "bacon" from list *mutation*
menu2 = ["eggs", "eggs", "bacon", "sausage", "spam"]
list(filter(lambda x: x != "eggs", menu)) # more like programming in Racket
list(filter(lambda x: x not in ["eggs" "bacon"], menu))

# Some more things
"spam" in menu
["green", "eggs"] + ["and", "ham"]
menu.reverse() # mutate

# dicts
a_dict = {}
a_dict["key1"] = [1,2,3]
a_dict["key2"] = "Michelle"
a_dict["key3"] = 10

# not immutable but if you have restraint...
backpack = {"front-pocket": "journal", "big-pocket":"laptop", "small-pocket":"pencils"} 


# referencing keys
a_dict.keys()

"front-pocket" in backpack.keys()
