#lang racket

;; Some Math
(+ 1 1)
(/ 10 2)
(expt 5 2)
(* 6 3)
(- 12 6)
(+ (* 8 6) 12)

;; Booleans
#t
#f
;; Anything not #f

;; Strings
"Hello World"
"Aretha \"The Queen of Soul\" Franklin"
(string-append "Fee " "Fi " "Fo " "Fum")
(string-ref "Blacksmith" 2) ; character is not a string #\a is not "a"
(printf "This is some text it has~n ~a~n" "Some Lines")
(string-length "grape")
(substring "restaurant" 0 4) ; not inclusive end position
(println "onomatopoeia")

;; lists 
(define menu '("eggs" "bacon" "sausage" "spam")) ; define is a function that sets a variable
(list-ref menu  2)
(length menu)
(last menu) ; there is also (first menu)


;; removing things from lists
(remove "bacon" menu) ; does not mutate original list only and removes *first* instance
(define menu2 (cons "eggs" menu)) ; '("eggs" "eggs" "bacon" "sausage" "spam") ; adds eggs to front of list
(remove* '("eggs")  menu2) ; remove all instances of eggs
(remove* '("eggs" "bacon")  menu2) ; no mutation

;; Some more things
(member "spam" menu)
(append (list "green" "eggs") (list "and" "ham"))
(reverse menu)

;; dicts
(define a-dict (make-hash)) ; mutable hash 
(hash-set! a-dict 'key1 '(1 2 3))
(hash-set! a-dict 'key2 "Michelle")
(hash-set! a-dict 'key3 10)

;; immutable hashes
(define backpack (hash 'front-pocket "journal" 'big-pocket "laptop" 'small-pocket "pencils"))

;; referencing keys
(hash-keys a-dict)

(hash-has-key? backpack 'front-pocket)


